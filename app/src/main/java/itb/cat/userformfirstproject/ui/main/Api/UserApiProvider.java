package itb.cat.userformfirstproject.ui.main.Api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserApiProvider {
    static UserApi userApi;

    public static UserApi getUserApi(){
        if(userApi==null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://itb.mateuyabar.com/DAM-M07/UF1/exercicis/userform/fakeapi/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            userApi = retrofit.create(UserApi.class);
        }
        return userApi;
    }
}
