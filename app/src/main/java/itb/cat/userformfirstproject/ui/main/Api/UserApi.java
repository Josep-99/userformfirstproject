package itb.cat.userformfirstproject.ui.main.Api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface UserApi {
    @GET("{username}/login.json")
    Call<UserSession> login(@Path("username") String username);

    @GET("{username}/register.json")
    Call<UserSession> register(@Path("username") String username);

}
