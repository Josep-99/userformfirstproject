package itb.cat.userformfirstproject.ui.main;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.textfield.TextInputLayout;

import itb.cat.userformfirstproject.R;

public class LoginFragment extends Fragment implements View.OnClickListener {

    Button registerButton;
    Button loginButton;
    TextInputLayout username;
    TextInputLayout password;

    private MainViewModel mViewModel;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        /*
        mViewModel.loading.observe(this,this::isloaging);
        mViewModel.logged.observe(this,this::isLogged);
        mViewModel.error.observe(this,this::isError);

         */
    }

    private void isloaging(Boolean aBoolean) {
        ProgressDialog progress = new ProgressDialog(getContext());

        if(aBoolean){
            progress = ProgressDialog.show(getContext(), "Loggeando...", "Por favor, espere", true);
            progress.show();
        }else{
            progress.dismiss();
        }
    }


    private void isError(String s) {
    }


    private void isLogged(Boolean aBoolean) {
        if(aBoolean){
            Navigation.findNavController(this.getView()).navigate(R.id.action_loginFragment_to_helloWorld);

        }else{

        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        username = getView().findViewById(R.id.editText);
        password = getView().findViewById(R.id.editText2);


        username = view.findViewById(R.id.editText);
        password = view.findViewById(R.id.editText2);
        view.findViewById(R.id.SecondLoginButton).setOnClickListener(this::enviar);
        view.findViewById(R.id.SecondRegisterButton).setOnClickListener(this::register);

    }
/*
    private void enviar(View view) {

        mViewModel.login("user1");


    }
    */

    private void register(View view) {
        Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_registerFragment);

    }

    private void enviar(View view) {
        if (CorrectInfo()) {
            Toast.makeText(getContext(), "Bien", Toast.LENGTH_LONG).show();
            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_helloWorld);
        } else {
            Toast.makeText(getContext(), "Datos erroneos, corrijelos", Toast.LENGTH_LONG).show();
        }
    }

    private boolean CorrectInfo() {
        boolean valido = true;

        if (username.getEditText().getText().toString().isEmpty()) {
            username.setError("El campo username es obligatorio");
            valido = false;
        }
        if (password.getEditText().getText().toString().isEmpty()) {
            password.setError("El campo password es obligatorio");
            valido = false;

        } else {
            return valido;
        }
        return valido;
    }

    @Override
    public void onClick(View view) {
        /*
        switch (view.getId()) {
            case R.id.SecondRegisterButton:
                Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_registerFragment);
                break;
            case R.id.SecondLoginButton:
                Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_helloWorld);
                break;
        }

         */
    }
}
