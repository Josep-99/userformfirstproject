package itb.cat.userformfirstproject.ui.main;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputLayout;

import java.lang.reflect.Array;
import java.util.Date;

import itb.cat.userformfirstproject.R;

public class RegisterFragment extends Fragment implements View.OnClickListener{

    private MainViewModel mViewModel;
    Button RegisterButton;
    TextInputLayout username;
    TextInputLayout password;
    TextInputLayout repeatPassword;
    TextInputLayout email;
    TextInputLayout name;
    TextInputLayout surname;
    TextInputLayout birthday;
    TextInputLayout genderPronoun;



    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        username = getView().findViewById(R.id.usernameRegister);
        password = getView().findViewById(R.id.passwordRegister);
        repeatPassword = getView().findViewById(R.id.repeatPasswordRegister);
        email = getView().findViewById(R.id.emailRegister);
        name = getView().findViewById(R.id.nameRegister);
        surname = getView().findViewById(R.id.surnameRegister);
        birthday =  getView().findViewById(R.id.birthDateResgister);
        birthday.getEditText().setOnClickListener(this::selectDate);
        genderPronoun = getView().findViewById(R.id.genderPronounRegister);
        genderPronoun.getEditText().setOnClickListener(this::seleccionarGender);


        //Poder conseguir la informacion del register_fragments
        username = view.findViewById(R.id.usernameRegister);
        password = view.findViewById(R.id.passwordRegister);
        repeatPassword = view.findViewById(R.id.repeatPasswordRegister);
        email = view.findViewById(R.id.emailRegister);
        name = view.findViewById(R.id.nameRegister);
        surname = view.findViewById(R.id.surnameRegister);
        view.findViewById(R.id.SecondRegisterButton).setOnClickListener(this::enviar);
    }

    //Al hacer click en el segundo boton de registro, se comprobará que los datos son correctos
    private void enviar(View view) {
        if(CorrectInfo()){
            Toast.makeText(getContext(),"Bien",Toast.LENGTH_LONG).show();
            Navigation.findNavController(view).navigate(R.id.action_registerFragment_to_helloWorld);
        }else
        {
            Toast.makeText(getContext(),"Datos erroneos, corrijelos",Toast.LENGTH_LONG).show();
        }
    }


    //El comprobador de los datos, por llamarlo de alguna manera, es el encargado de devolver un boolen para ver si se pasa al
    //siguiente fragment o no, en caso de que los datos esten erroneos, devolverá un false
    private boolean CorrectInfo() {
        boolean valido = true;

        if(username.getEditText().getText().toString().isEmpty()){
            username.setError("El campo username es obligatorio");
            valido = false;

        }if(password.getEditText().getText().toString().isEmpty()){
            password.setError("El campo password es obligatorio");
            valido = false;

        }if(!password.getEditText().getText().toString().equals(repeatPassword.getEditText().getText().toString())){
            password.setError("Las contraseñas no coinciden");
            repeatPassword.setError("Las contraseñas no coinciden");
            valido = false;

        }if(email.getEditText().getText().toString().isEmpty()) {
            email.setError("El campo email es obligatorio");
            valido = false;

        }if(birthday.getEditText().getText().toString().isEmpty()) {
            birthday.setError("El campo birthday es obligatorio");
            valido = false;

        }if(genderPronoun.getEditText().getText().toString().isEmpty()){
                genderPronoun.setError("El campo gender pronoun es obligatorio");
            valido = false;

        }else{
            return valido;
        }
        return valido;
    }

    private void seleccionarGender(View view) {

        String optionArray[] = new String[]{"Male","Female","Neutral","I prefer not to say"};

        new MaterialAlertDialogBuilder(getContext())
                .setTitle(R.string.gender)
                .setItems(optionArray, /* listener */new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        genderPronoun.getEditText().setText(optionArray[i]);
                    }
                })
                .show();
    }


    private void selectDate(View view) {
        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText(R.string.birthDate);
        MaterialDatePicker<Long> picker = builder.build();
        picker.addOnPositiveButtonClickListener(this::doOnDateSelected);
        picker.show(getFragmentManager(), picker.toString());
    }

    //Clase que devolverá la fecha, hay que pasarlo al texto
    private void doOnDateSelected(Long aLong) {
        Date fechaCorrecta = new Date(aLong);

        String fecha = fechaCorrecta.toString();
        birthday.getEditText().setText(fecha);
    }


    @Override
    public void onClick(View view) {
        /*
        switch (view.getId()){
            case R.id.SecondRegisterButton:
                Toast.makeText(getContext(),"Hey",Toast.LENGTH_LONG).show();
                break;
        }
         */
    }
}