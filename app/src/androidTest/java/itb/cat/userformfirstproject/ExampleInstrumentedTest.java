package itb.cat.userformfirstproject;

import androidx.test.espresso.action.ViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class);


    @Test
    public void TestLoginCorrecto(){
        onView(withId(R.id.FirstLoginButton)).perform(click());
        onView(withId(R.id.textlogin1)).perform(click()).perform(typeText("username"));
        onView(withId(R.id.textpassword1)).perform(click()).perform(typeText("password"), ViewActions.closeSoftKeyboard()); ;
        onView(withId(R.id.SecondLoginButton)).perform(click());
        onView(withId(R.id.buttonLogOut)).perform(click());
    }

    @Test
    public void TestLoginIncorrecto(){
        onView(withId(R.id.FirstLoginButton)).perform(click());
        onView(withId(R.id.textlogin1)).perform(click()).perform(typeText(""));
        onView(withId(R.id.textpassword1)).perform(click()).perform(typeText(""), ViewActions.closeSoftKeyboard()); ;
        onView(withId(R.id.SecondLoginButton)).perform(click());
    }

    @Test
    public void TestRegistreCorrecto(){
        onView(withId(R.id.FirstRegisterButton)).perform(click());
        onView(withId(R.id.username)).perform(click()).perform(typeText("username"));
        onView(withId(R.id.password)).perform(click()).perform(typeText("password"));
        onView(withId(R.id.repeatPassword)).perform(click()).perform(typeText("password"));
        onView(withId(R.id.email)).perform(click()).perform(typeText("email@email"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.name)).perform(click()).perform(typeText("name"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.surname)).perform(click()).perform(typeText("surname"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.SecondRegisterButton)).perform(scrollTo(),click());
    }

    @Test
    public void TestRegistreIncorrecto(){
        onView(withId(R.id.FirstRegisterButton)).perform(click());
        onView(withId(R.id.username)).perform(click()).perform(typeText("badusername"));
        onView(withId(R.id.password)).perform(click()).perform(typeText("password"));
        onView(withId(R.id.repeatPassword)).perform(click()).perform(typeText("password2"));
        onView(withId(R.id.email)).perform(click()).perform(typeText(""), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.name)).perform(click()).perform(typeText("name"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.surname)).perform(click()).perform(typeText("surname"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.SecondRegisterButton)).perform(scrollTo(),click());
    }

    @Test
    public void TestLoginRegistreFromLogin(){
        onView(withId(R.id.FirstLoginButton)).perform(click());
        onView(withId(R.id.SecondRegisterButton)).perform(click());
        onView(withId(R.id.username)).perform(click()).perform(typeText("username"));
        onView(withId(R.id.password)).perform(click()).perform(typeText("password"));
        onView(withId(R.id.repeatPassword)).perform(click()).perform(typeText("password"));
        onView(withId(R.id.email)).perform(click()).perform(typeText("email@email"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.name)).perform(click()).perform(typeText("name"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.surname)).perform(click()).perform(typeText("surname"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.SecondRegisterButton)).perform(scrollTo(),click());

    }
}